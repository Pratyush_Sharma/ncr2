package com.chainreactionai.game2;

/**
 * Created by pratyushsharma on 13/08/15.
 */

/** Provides bit flag constants for alignment.
 * @author Nathan Sweet */
public class Align {
    static public final int center = 1 << 0;
    static public final int top = 1 << 1;
    static public final int bottom = 1 << 2;
    static public final int left = 1 << 3;
    static public final int right = 1 << 4;
}
