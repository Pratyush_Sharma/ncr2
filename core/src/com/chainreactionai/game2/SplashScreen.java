/**
 * 
 */
package com.chainreactionai.game2;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

/**
 * @author Parnami
 * 
 */
public class SplashScreen implements Screen {
	SpriteBatch batch;
	private OrthographicCamera camera;
	private ChainReaction2 myGame;
	private Texture splashScreenBackground;
	final private int WIDTH_SCREEN = 448;
	final private int HEIGHT_SCREEN = 645;
	final boolean MONTE_CARLO = false;
	private long prevTime, newTime;
	private Image img;
	private Stage stage = new Stage();
//	private PrintWriter out;

	public SplashScreen(ChainReaction2 game) {
		prevTime = System.currentTimeMillis();
		
		// Monte Carso Simulations Stuff
//		if (MONTE_CARLO) {
//			try {
//				out = new PrintWriter("monteCarloResults1.txt");
//			} catch (FileNotFoundException e) {
//				e.printStackTrace();
//			}
//			int numPlayers = 2;
//			int[] difficultyLevelList = new int[numPlayers]; int[] heuristicNumber = new int[numPlayers];
//			difficultyLevelList[0] = 1; difficultyLevelList[1] = 4;
////			difficultyLevelList[2] = 1; difficultyLevelList[3] = 2;
//			heuristicNumber[0] = 12; heuristicNumber[1] = 12;
////			heuristicNumber[2] = 12; heuristicNumber[3] = 12;
//			MonteCarlo monteCarlo;
//			for (int i = 0; i < 2; ++i)
//			{
//				for (int j = 0; j < 2; ++j)
//				{
//					difficultyLevelList[0] = i; difficultyLevelList[1] = j;
////					System.out.println("\nLevel "+i+" vs Level "+j);
//					monteCarlo = new MonteCarlo(numPlayers, difficultyLevelList, heuristicNumber, true, out);
//					if (i+j > 10)
//						monteCarlo.runSimulations(100);
//					else
//						monteCarlo.runSimulations(200);
//				}
//			}
////			monteCarlo = new MonteCarlo(numPlayers, difficultyLevelList, heuristicNumber, true);		
////			monteCarlo.runSimulations(1000);
//			out.close();
//			Gdx.app.exit();
//		}
		myGame = game;
		create();
	}

	// Initialization function
	private void create() {
		batch = new SpriteBatch();
		// Show the world to be 440*480 no matter the
		// size of the screen
		camera = new OrthographicCamera();
		camera.setToOrtho(false, WIDTH_SCREEN, HEIGHT_SCREEN);
		// Load the SplashScreen image.
		splashScreenBackground = new Texture("SplashScreen.jpg");
		img = new Image(splashScreenBackground);
		img.setFillParent(true);
		stage.addActor(img);
		Gdx.input.setInputProcessor(stage);
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		newTime = System.currentTimeMillis();
		// Tell the camera to update its matrices.
		camera.update();
		stage.act();
		stage.draw();

		// When user clicks on the screen, go to the main menu screen
		if (newTime - prevTime > 1000) {
			// Load the imageButton textures which were previously loaded in ChainReaction2.java
			loadAllTextures();
			if (!ChainReaction2.GRAYED_OUT)
				myGame.setScreen(new MainMenuScreen(myGame));
			else
				myGame.setScreen(new TutorialTextScreen(myGame, 1));
		}
	}
	
	void loadAllTextures() {
		
		// Load the drawables for image buttons
		ChainReaction2.achievementsButtonDraw = (Drawable)(new TextureRegionDrawable(new TextureRegion(new Texture("buttons/achievements.jpg"))));
		ChainReaction2.achievementsGrayButtonDraw = (Drawable)(new TextureRegionDrawable(new TextureRegion(new Texture("buttons/achievementsGray.jpg"))));
		ChainReaction2.achievementsPressedButtonDraw = (Drawable)(new TextureRegionDrawable(new TextureRegion(new Texture("buttons/achievementsPressed.png"))));
		ChainReaction2.backButtonDraw = (Drawable)(new TextureRegionDrawable(new TextureRegion(new Texture("buttons/backb.jpg"))));
		ChainReaction2.backGrayButtonDraw = (Drawable)(new TextureRegionDrawable(new TextureRegion(new Texture("buttons/backbGray.jpg"))));
		ChainReaction2.backPressedButtonDraw = (Drawable)(new TextureRegionDrawable(new TextureRegion(new Texture("buttons/backbPressed.png"))));
		ChainReaction2.exitButtonDraw = (Drawable)(new TextureRegionDrawable(new TextureRegion(new Texture("buttons/exit.jpg"))));
		ChainReaction2.exitGrayButtonDraw = (Drawable)(new TextureRegionDrawable(new TextureRegion(new Texture("buttons/exitGray.jpg"))));
		ChainReaction2.exitPressedButtonDraw = (Drawable)(new TextureRegionDrawable(new TextureRegion(new Texture("buttons/exitPressed.png"))));
		ChainReaction2.leaderboardButtonDraw = (Drawable)(new TextureRegionDrawable(new TextureRegion(new Texture("buttons/leaderboard.jpg"))));
		ChainReaction2.leaderboardGrayButtonDraw = (Drawable)(new TextureRegionDrawable(new TextureRegion(new Texture("buttons/leaderboardGray.jpg"))));
		ChainReaction2.leaderboardPressedButtonDraw = (Drawable)(new TextureRegionDrawable(new TextureRegion(new Texture("buttons/leaderboardPressed.png"))));
		ChainReaction2.mainMenuButtonDraw = (Drawable)(new TextureRegionDrawable(new TextureRegion(new Texture("buttons/mainMenu.jpg"))));
		ChainReaction2.mainMenuGrayButtonDraw = (Drawable)(new TextureRegionDrawable(new TextureRegion(new Texture("buttons/mainMenuGray.jpg"))));
		ChainReaction2.mainMenuPressedButtonDraw = (Drawable)(new TextureRegionDrawable(new TextureRegion(new Texture("buttons/mainMenuPressed.png"))));
		ChainReaction2.newGameButtonDraw = (Drawable)(new TextureRegionDrawable(new TextureRegion(new Texture("buttons/newGame.jpg"))));
		ChainReaction2.newGameGrayButtonDraw = (Drawable)(new TextureRegionDrawable(new TextureRegion(new Texture("buttons/newGameGray.jpg"))));
		ChainReaction2.newGamePressedButtonDraw = (Drawable)(new TextureRegionDrawable(new TextureRegion(new Texture("buttons/newGamePressed.png"))));
		ChainReaction2.pauseButtonDraw = (Drawable)(new TextureRegionDrawable(new TextureRegion(new Texture("buttons/pause.jpg"))));
		ChainReaction2.pauseGrayButtonDraw = (Drawable)(new TextureRegionDrawable(new TextureRegion(new Texture("buttons/pauseGray.jpg"))));
		ChainReaction2.pausePressedButtonDraw = (Drawable)(new TextureRegionDrawable(new TextureRegion(new Texture("buttons/pausePressed.png"))));
		ChainReaction2.playButtonDraw = (Drawable)(new TextureRegionDrawable(new TextureRegion(new Texture("buttons/play.jpg"))));
		ChainReaction2.playGrayButtonDraw = (Drawable)(new TextureRegionDrawable(new TextureRegion(new Texture("buttons/playGray.jpg"))));
		ChainReaction2.playPressedButtonDraw = (Drawable)(new TextureRegionDrawable(new TextureRegion(new Texture("buttons/playPressed.png"))));
		ChainReaction2.playAgainButtonDraw = (Drawable)(new TextureRegionDrawable(new TextureRegion(new Texture("buttons/playAgain.jpg"))));
		ChainReaction2.playAgainGrayButtonDraw = (Drawable)(new TextureRegionDrawable(new TextureRegion(new Texture("buttons/playAgainGray.jpg"))));
		ChainReaction2.playAgainPressedButtonDraw = (Drawable)(new TextureRegionDrawable(new TextureRegion(new Texture("buttons/playAgainPressed.png"))));
		ChainReaction2.resumeButtonDraw = (Drawable)(new TextureRegionDrawable(new TextureRegion(new Texture("buttons/resume.jpg"))));
		ChainReaction2.resumeGrayButtonDraw = (Drawable)(new TextureRegionDrawable(new TextureRegion(new Texture("buttons/resumeGray.jpg"))));
		ChainReaction2.resumePressedButtonDraw = (Drawable)(new TextureRegionDrawable(new TextureRegion(new Texture("buttons/resumePressed.png"))));
		ChainReaction2.rulesButtonDraw = (Drawable)(new TextureRegionDrawable(new TextureRegion(new Texture("buttons/rules.jpg"))));
		ChainReaction2.rulesGrayButtonDraw = (Drawable)(new TextureRegionDrawable(new TextureRegion(new Texture("buttons/rulesGray.jpg"))));
		ChainReaction2.rulesPressedButtonDraw = (Drawable)(new TextureRegionDrawable(new TextureRegion(new Texture("buttons/rulesPressed.png"))));
		ChainReaction2.submitButtonDraw = (Drawable)(new TextureRegionDrawable(new TextureRegion(new Texture("buttons/submit.jpg"))));
		ChainReaction2.submitGrayButtonDraw = (Drawable)(new TextureRegionDrawable(new TextureRegion(new Texture("buttons/submitGray.jpg"))));
		ChainReaction2.submitPressedButtonDraw = (Drawable)(new TextureRegionDrawable(new TextureRegion(new Texture("buttons/submitPressed.png"))));
		ChainReaction2.tutorialButtonDraw = (Drawable)(new TextureRegionDrawable(new TextureRegion(new Texture("buttons/tutorial.jpg"))));
		ChainReaction2.tutorialGrayButtonDraw = (Drawable)(new TextureRegionDrawable(new TextureRegion(new Texture("buttons/tutorialGray.jpg"))));
		ChainReaction2.tutorialPressedButtonDraw = (Drawable)(new TextureRegionDrawable(new TextureRegion(new Texture("buttons/tutorialPressed.png"))));
		ChainReaction2.pressedHumanCpuButtonDraw = (Drawable)(new TextureRegionDrawable(new TextureRegion(new Texture("buttons/pressedHumanCpu.png"))));
		ChainReaction2.unpressedHumanButtonDraw = (Drawable)(new TextureRegionDrawable(new TextureRegion(new Texture("buttons/unpressedHuman.png"))));
		ChainReaction2.unpressedCpuButtonDraw = (Drawable)(new TextureRegionDrawable(new TextureRegion(new Texture("buttons/unpressedCPU.png"))));
		ChainReaction2.logoDraw = (Drawable)(new TextureRegionDrawable(new TextureRegion(new Texture("Logo.png"))));
		ChainReaction2.nextButtonDraw = (Drawable)(new TextureRegionDrawable(new TextureRegion(new Texture("buttons/next.jpg"))));
		ChainReaction2.nextGrayButtonDraw = (Drawable)(new TextureRegionDrawable(new TextureRegion(new Texture("buttons/nextGray.jpg"))));
		ChainReaction2.nextPressedButtonDraw = (Drawable)(new TextureRegionDrawable(new TextureRegion(new Texture("buttons/nextPressed.png"))));
		ChainReaction2.skipButtonDraw = (Drawable)(new TextureRegionDrawable(new TextureRegion(new Texture("buttons/skip.jpg"))));
		ChainReaction2.skipPressedButtonDraw = (Drawable)(new TextureRegionDrawable(new TextureRegion(new Texture("buttons/skipPressed.png"))));
		ChainReaction2.muteActivateButton = (Drawable)(new TextureRegionDrawable(new TextureRegion(new Texture("buttons/volume51gold.png"))));
		ChainReaction2.muteInactivateButton = (Drawable)(new TextureRegionDrawable(new TextureRegion(new Texture("buttons/volume47gold.png"))));
		
		// Loading game background image
		ChainReaction2.texture = new Texture(Gdx.files.internal("back.jpg"));
		ChainReaction2.textureGray = new Texture(Gdx.files.internal("backGray.jpg"));
		ChainReaction2.mainGameScreenTexture = new Texture(Gdx.files.internal("mainGameScreenBack.jpg"));
				
		// Loading the skin
		ChainReaction2.skin = new Skin(Gdx.files.internal("data/uiskin.json"),
				new TextureAtlas(Gdx.files.internal("data/uiskin.atlas")));
		ChainReaction2.sliderSkin = new Skin(Gdx.files.internal("data/ui-color.json"),
				new TextureAtlas(Gdx.files.internal("data/ui-yellow.atlas")));
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}
}
